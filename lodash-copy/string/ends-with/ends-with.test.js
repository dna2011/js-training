const endsWith = require('./ends-with');

describe('EndsWith', () => {
  it.each`
    str       | char   | expected
    ${'abcd'} | ${'d'} | ${true}
    ${'abcd'} | ${'c'} | ${false}
    ${'abcd'} | ${''}  | ${true}
    ${''}     | ${''}  | ${false}
    ${''}     | ${'a'} | ${false}
  `(
    'should return $expected when look for $char in $str',
    ({ str, char, expected }) => {
      const result = endsWith(str, char);

      expect(result).toBe(expected);
    }
  );
});
