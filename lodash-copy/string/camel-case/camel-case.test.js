const camelCase = require('./camel-case');

describe('CamelCase', () => {
  const expected = 'helloWorld';

  it.each`
    str                  | stringDescription       | expected
    ${'HelloWorld'}      | ${'PascalCase'}         | ${expected}
    ${'hello-world'}     | ${'kebab-case'}         | ${expected}
    ${'Hello world'}     | ${'space separated'}    | ${expected}
    ${'__HELLO_WORLD__'} | ${'__GLOBAL_CONST__'}   | ${expected}
    ${'--hello-world--'} | ${'--dash-separated--'} | ${expected}
  `(
    'should return $expected when $stringDescription string provided',
    ({ str, expected }) => {
      const result = camelCase(str);

      expect(result).toBe(expected);
    }
  );
});
