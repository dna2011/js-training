const lowerFirst = require('./lower-first');

describe('LowerFirst', () => {
  it('should return same string with lower case first char', () => {
    const str = 'Hello';
    const strLowerCase = 'hello';
    const expected = 'hello';

    const result1 = lowerFirst(str);
    const result2 = lowerFirst(strLowerCase);

    expect(result1).toBe(expected);
    expect(result2).toBe(expected);
  });

  it('should return empty string if empty string is provided', () => {
    const str = '';
    const expected = '';

    const result = lowerFirst(str);

    expect(result).toBe(expected);
  });

  it('should set empty string as default argument', () => {
    const expected = '';

    const result = lowerFirst(undefined);

    expect(result).toBe(expected);
  });
});
