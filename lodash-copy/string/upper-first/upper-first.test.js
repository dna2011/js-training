const upperFirst = require('./upper-first');

describe('UpperFirst', () => {
  it('should return the same string with first letter in upper case', () => {
    const str = 'hello';
    const expected = 'Hello';

    const result = upperFirst(str);

    expect(result).toBe(expected);
  });

  it('should return the same string if first letter is in upper case', () => {
    const str = 'Hello';

    const result = upperFirst(str);

    expect(result).toBe(str);
  });

  it('should return empty string if emptry string is provided', () => {
    const str = '';

    const result = upperFirst(str);

    expect(result).toBe(str);
  });
});
