const join = require('./join')

describe('Join', () => {
  const ARRAY = [1, 2, 3, 4]
  const DEFAULT_SEPARATOR_STRING = '1,2,3,4'
  const SEPARATOR = '-'
  const STRING = '1-2-3-4'

  it('should return separator-joined string', () => {
    const result = join(ARRAY, SEPARATOR)

    expect(result).toEqual(STRING)
  })

  it('should return string with default separator, when it it not provided', () => {
    const result = join(ARRAY)

    expect(result).toEqual(DEFAULT_SEPARATOR_STRING)
  })
})