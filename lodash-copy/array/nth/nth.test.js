const nth = require('./nth')

describe('Nth', () => {
  it('should return array element at provided index', () => {
    const array = [1, 2, 3, 4]
    const index = 2
    const expected = 3

    const result = nth(array, index)

    expect(result).toBe(expected)
  })

  it('should return first element if `index` not provided', () => {
    const array = [1, 2, 3, 4]
    const expected = 1

    const result = nth(array)

    expect(result).toBe(expected)
  })

  it('should return nth element from end if `index` is negative', () => {
    const array = [1, 2, 3, 4]
    const index1 = -1
    const index2 = -2
    const expected1 = 4
    const expected2 = 3

    const result1 = nth(array, index1)
    const result2 = nth(array, index2)

    expect(result1).toBe(expected1)
    expect(result2).toBe(expected2)
  })
})