const compact = require('./compact');

describe('Compact', () => {
  it('should return compacted array', () => {
    const falsey = [, null, undefined, false, 0, NaN, ''];
    const truthly = ['a', 1, {}, [], Infinity, true, () => {}];
    const array = falsey.concat(truthly);

    const result = compact(array);

    expect(result).toEqual(truthly);
  });
});
