const chunk  = require('./chunk')
const falsey = [, null, undefined, false, 0, NaN, '']

describe('Chunk', () => {
  it('returns chunked arrays', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const CHUNK_SIZE = 3
    const CHUNKED_ARRAY = [[1, 2, 3], [4, 5, 6]]
    const result = chunk(ARRAY, CHUNK_SIZE)

    expect(result).toEqual(CHUNKED_ARRAY)
  })

  it('returns remaining elements as last chunk', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const CHUNK_SIZE = 4;
    const CHUNKED_ARRAY = [[1, 2, 3, 4], [5, 6]]
    const result = chunk(ARRAY, CHUNK_SIZE)

    expect(result).toEqual(CHUNKED_ARRAY)
  })

  it('returns an empty array when `size` = 0', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const result = chunk(ARRAY, 0)
    
    expect(result).toEqual([])
  })

  it('sets 1 as default `size`', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const CHUNKED_ARRAY = [[1], [2], [3], [4], [5], [6]]
    const result = chunk(ARRAY)
    
    expect(result).toEqual(CHUNKED_ARRAY)
  })

  it('should treat falsey `size` as 0, except `undefined`', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const result = falsey.map(el => el === undefined ? chunk(ARRAY) : chunk(ARRAY, el));
    const expectedResult = falsey.map(el => el === undefined ? [[1], [2], [3], [4], [5], [6]] : [])

    expect(result).toEqual(expectedResult)
  })

  it('should round down any non-integer size', () => {
    const ARRAY = [1, 2, 3, 4, 5, 6]
    const CHUNKED_ARRAY = [[1, 2, 3], [4, 5, 6]]
    const result = chunk(ARRAY, 3.6)

    expect(result).toEqual(CHUNKED_ARRAY)
  })
})